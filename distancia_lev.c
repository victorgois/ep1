#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define MAX_LENGTH 30 //definindo palavras com no maximo 30 caracteres

static int distancia ( char *palavra1, int len1, char *palavra2, int len2){

    char c1,c2;
    int i, j;
    int matrix[len1 + 1][len2 + 1];
    
    for (i = 0; i<= len1; i++){
        matrix[i][0] = i;
    }
    for (i=0; i<= len2; i++){   //inicializando minha matriz com valores 0 a lenght-1
            matrix[0][i] = i;
    }
   
    for (i = 1; i<=len1; i++){
        c1 = palavra1[i-1]; 
        // Dentro deste loop, eu armazeno os caracteres da primeira palavra
        for (j=1; j<=len2; j++){
            c2 = palavra2[j-1];
        // Dentro deste, por sua vez, armazeno os caracteres da segunda palavra
            if (c1==c2){
                matrix[i][j] = matrix[i-1][j-1]; 
            /* Aqui, ainda dentro do loop, faco a comparacao de todos os caracteres das duas palavras.
            Caso elas sejam iguais, nao havera operacao.
            */
            }
            else {
                // Operacoes:
                int del;
                int ins;
                int subs;
                int min;

                del = matrix[i-1][j] + 1;
                ins = matrix[i][j-1] + 1;
                subs = matrix[i-1][j-1] + 1;
                min = del ;

                if (ins < min) {
                    min = ins;
                }
                if (subs < min){
                    min = subs;
                }
                matrix[i][j] = min;
            }
        }
    }
    return matrix [len1][len2];
}

int main (){

  //  char *palavra1;
  //  char *palavra2;
    char *palavra1 = malloc (MAX_LENGTH); // alocando valor maximo de 30 caracteres para palavras
    char *palavra2 = malloc (MAX_LENGTH);
    int len1;
    int len2;
    int distancia_real;

    printf ("\nEntre com a primeira palavra: \n");
    scanf ("%s", palavra1);
    printf ("\nPalavra Salva! Agora, entre com a segunda: \n\n");
    scanf ("%s", palavra2);

    len1 = strlen (palavra1);
    len2 = strlen (palavra2);
    distancia_real = distancia(palavra1, len1, palavra2, len2);
    printf ("\nA distancia de edicao entre %s e %s e de %d. \n", palavra1, palavra2, distancia_real);

    return 0;
    
}

